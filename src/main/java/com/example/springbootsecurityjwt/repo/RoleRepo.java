package com.example.springbootsecurityjwt.repo;

import com.example.springbootsecurityjwt.domain.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepo extends JpaRepository<Role, Long> {
    Role findByName(String name);
}
