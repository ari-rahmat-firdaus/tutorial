package com.example.springbootsecurityjwt.repo;

import com.example.springbootsecurityjwt.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepo extends JpaRepository<User, Long> {
    User findByUsername(String username);
}
