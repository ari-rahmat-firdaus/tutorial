package com.example.springbootsecurityjwt.api;

import com.example.springbootsecurityjwt.domain.Role;
import com.example.springbootsecurityjwt.domain.User;
import com.example.springbootsecurityjwt.service.UserService;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("api")
@AllArgsConstructor
public class UserResource {
    private final UserService userService;

    @GetMapping("users")
    public ResponseEntity<List<User>> getUsers() {
        return ResponseEntity.ok().body(userService.getUsers());
    }

    @PostMapping("users")
    public ResponseEntity<User> saveUser(@RequestBody User user) {
        URI uri = URI.create(
                ServletUriComponentsBuilder
                        .fromCurrentContextPath().path("/api/users").toString());
        return ResponseEntity.created(uri).body(userService.saveUser(user));
    }

    @PostMapping("roles")
    public ResponseEntity<Role> addRoleToUser(@RequestBody Role role) {
        URI uri = URI.create(
                ServletUriComponentsBuilder
                        .fromCurrentContextPath().path("/api/roles").toString());
        return ResponseEntity.created(uri).body(userService.saveRole(role));
    }

    @PostMapping("roles/addtouser")
    public ResponseEntity<?> saveRole(@RequestBody UserRoleForm form) {
        userService.addRoleToUser(form.getUsername(), form.getRoleName());
        return ResponseEntity.ok().build();
    }

}

@Data
class UserRoleForm {
    private String username;
    private String roleName;
}